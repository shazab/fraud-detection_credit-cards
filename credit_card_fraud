
import pandas as pd
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score,recall_score,f1_score,roc_curve, auc
from sklearn.metrics import confusion_matrix
from pandas_ml import ConfusionMatrix
creditcard = pd.read_csv('Documents/BDA 106 - Datasets/creditcard.csv')
#Reading the first few lines of data
creditcard.head(10)
#Column time denotes time from the first transaction. V1 to V28 represents confidential information which seems to have 
#been normalized. 
#column amount represents the transaction money for that specific time 
#class represents whether the transaction is fraud (label:1) or not (0)
#Getting a sense of the data 
creditcard.info()
creditcard.describe()
#checking for missing cell/data
creditcard.isnull().sum(axis = 0)
#checking the number of unique values in class column 
creditcard['Class'].unique() #there are 0s and 1s
#Getting the number of 0s and 1s in that column 
pd.value_counts(creditcard['Class'].values, sort=False)
print('The percentage of fraudulent transactions are:{:.3f}'.format((creditcard['Class']==1).sum()/len(creditcard['Class'])*100),'%')
print('The percentage of non fraudulent transactions are:{:.3f}'.format((creditcard['Class']==0).sum()/len(creditcard['Class'])*100),'%')
#As can be seen, this is a case of highly imbalanced data. There are several approaches to dealing with this data and 
#build a model that will be able to detect these fraudulent transactions which right now will look as outliers.
#The first approach is to do nothing and adjust the class_weights = balanced and look at the recall, F1_scores as well as the confusion matrix
#Accuracy scores do not work well when working with imbalanced data
#Prior to running the models, time and amount columns will be standardized since columns from V1 to V28 have undergone PCA 
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler().fit(np.array(creditcard['Amount']).reshape(-1,1))
scaled_amount = scaler.transform(np.array(creditcard['Amount']).reshape(-1,1))
scaler1 = StandardScaler().fit(np.array(creditcard['Time']).reshape(-1,1))
scaled_time = scaler1.transform(np.array(creditcard['Time']).reshape(-1,1))
#Creating a 2nd dataframe with scaled amount and scaled time as columns. Will drop the original "time" and "amount" column
df = pd.DataFrame(scaled_amount,columns =['scaled_amount'])
df2 = pd.DataFrame(scaled_time,columns =['scaled_time'])
credit_card_ss = pd.concat([creditcard,df, df2], axis=1)
cols_to_drop =['Time','Amount']
credit_card_ss = credit_card_ss[credit_card_ss.columns.drop(cols_to_drop)]
credit_card_ss.head(5) #Checking that standardizing of specified columns have occurred, appended to a new dataset and dropping of "time" 
#amount columns
from sklearn.model_selection import train_test_split
X_credit_card_ss = credit_card_ss.drop('Class', axis=1)
y_credit_card_ss = credit_card_ss['Class']
X_credit_card_ss_train, X_credit_card_ss_test, y_credit_card_ss_train, y_credit_card_ss_test = train_test_split(X_credit_card_ss, y_credit_card_ss, test_size=0.3, random_state=55)
#Now we can use some classifiers on this dataset and check the scores. will adopt the balanced approach for the class weights to deal with the imbalanced dataset
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn import svm
import matplotlib.pyplot as plt
import seaborn as sns
​
lr1 = LogisticRegression(class_weight='balanced', random_state=40)
clf1 = DecisionTreeClassifier(criterion='gini', splitter='best', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features=None, random_state=60, max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, class_weight='balanced', presort=False)
svm1 = svm.SVC(kernel='rbf', C = 1.0, class_weight ='balanced', random_state=40)
#Training models
model_lr1 = lr1.fit(X_credit_card_ss_train,y_credit_card_ss_train)
model_clf1 = clf1.fit(X_credit_card_ss_train,y_credit_card_ss_train)
model_svm1 = svm1.fit(X_credit_card_ss_train,y_credit_card_ss_train)
#Prediction on test set 
pred_y_lr1 = lr1.predict(X_credit_card_ss_test)
pred_y_clf1 = clf1.predict(X_credit_card_ss_test)
pred_y_svm1 = svm1.predict(X_credit_card_ss_test)
#Printing recall and F1_scores 
print ('Recall score of logistic regression classifier on test set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_lr1)))
print ('F1 score of logistic regression classifier on test set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_lr1)))
cm_lr1 = confusion_matrix(y_credit_card_ss_test,pred_y_lr1)
print('Confusion matrix with logistic regression classifier with on test set:\n%s' % cm_lr1)
print('\n')
print ('Recall score of decision tree classifier on test set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_clf1)))
print ('F1 score of decision tree classifier on test set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_clf1)))
cm_clf1 = confusion_matrix(y_credit_card_ss_test,pred_y_clf1)
print('Confusion matrix with decision tree classifier with on test set:\n%s' % cm_clf1)
print('\n')
print ('Recall score of SVM classifier on test set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_svm1)))
print ('F1 score of SVM classifier on test set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_svm1)))
cm_svm1 = confusion_matrix(y_credit_card_ss_test,pred_y_svm1)
print('Confusion matrix with decision tree classifier with on test set:\n%s' % cm_svm1)
#As can be seen, both models have not done so well in predicting the fraud cases. In both models, they were mostly classified as 
# false negative ie non fraud cases. 
#USe cross validation score cross_val_score to redo the models and check for any improvements in recall and f1 scores
# Perform 6-fold cross validation
recall_lr1 = cross_val_score(lr1, X_credit_card_ss , y_credit_card_ss, scoring='recall', cv = 6)
print("Recall score for logistic regression with balanced class weight: %0.3f (+/- %0.3f)" % (recall_lr1.mean(), recall_lr1.std() * 2))
f1_lr1 = cross_val_score(lr1,X_credit_card_ss , y_credit_card_ss, scoring='f1', cv = 6)
print("F1 score for logistic regression with balanced class weight: %0.3f (+/- %0.3f)" % (f1_lr1.mean(), f1_lr1.std() * 2))
recall_clf1 = cross_val_score(clf1,X_credit_card_ss , y_credit_card_ss, scoring='recall', cv = 6)
print("Recall score for decision tree classifier with balanced class weight: %0.3f (+/- %0.3f)" % (recall_clf1.mean(), recall_clf1.std() * 2))
f1_clf1 = cross_val_score(clf1, X_credit_card_ss , y_credit_card_ss, scoring='f1', cv = 6)
print("F1 score for decision tree classifier with balanced class weight: %0.3f (+/- %0.3f)" % (f1_clf1.mean(), f1_clf1.std() * 2))
#Getting an idea of transaction amount over time
import matplotlib.pyplot as plt
x = creditcard['Time']
y = creditcard['Amount']
plt.title('Trend in transaction amount')
plt.xlabel('Time,s')
plt.ylabel('Amount')
plt.plot(x,y)
plt.show()
#Is there some correlation between the variables in the dataset 
import matplotlib.pyplot as plt
import seaborn as sns
plt.figure(figsize = (20,10))
sns.heatmap(credit_card_ss.corr(), annot=True, fmt=".2f")
plt.show()
#Based on the heat map, there are some negative correlations observed in the dataset for example: 
# time and v3, v2 and amount
#Two approaches to make a balanced dataset out of an imbalanced one are under-sampling and over-sampling.
​
#Under-sampling
#Under-sampling balances the dataset by reducing the size of the abundant class. This method is used when quantity of data is 
#sufficient. By keeping all samples in the rare class and randomly selecting an equal number of samples in the abundant class, 
#a balanced new dataset can be retrieved for further modelling.Generating centroid based on a clustering method (e.g. K-Means) 
#is a common strategy for this
​
#Oversampling
#On the contrary, oversampling is used when the quantity of data is insufficient. It tries to balance dataset by increasing 
#the size of rare samples. Rather than getting rid of abundant samples, new rare samples are generated by 
#using e.g. repetition, bootstrapping or SMOTE (Synthetic Minority Over-Sampling Technique)
#Using under sampling approach to create a balanced dataset
#Undersample the class = 0 which represents non fraudulent transactions
from sklearn.utils import resample
creditcard_nonfraud = credit_card_ss[credit_card_ss.Class==0]
creditcard_fraud = credit_card_ss[credit_card_ss.Class==1]
#Taking a percentage of fraudulent cases ~61%. Will select 300 cases of fraudulent activities without replacement and leave 
#192 cases in original dataset to models predict their class. 192 cases will be part of the unseen data.
creditcard_undersampled = resample(credit_card_ss[credit_card_ss.Class==1], 
                                 replace=False,    # sample without replacement
                                 n_samples=300,     # represent around 61% of fraudulent cases
                                 random_state=75) # reproducible results
​
# Undersample majority class
creditcard_nonfraud_undersampled = resample(credit_card_ss[credit_card_ss.Class==0], 
                                 replace=False,    # sample without replacement
                                 n_samples=300,     # to match minority class
                                 random_state=70) # reproducible results
 
# Combine minority class with downsampled majority class
creditcard_undersampled = pd.concat([creditcard_nonfraud_undersampled, creditcard_undersampled])
 
# Display new class counts
print(creditcard_undersampled.Class.value_counts())
​
​
#Creating new dataframe which does not contain the data that is present in creditcard_undersampled dataset
creditcard_original_leftover = credit_card_ss.loc[~credit_card_ss.set_index(list(credit_card_ss.columns)).index.isin(creditcard_undersampled.set_index(list(creditcard_undersampled.columns)).index)]
#Checking shape of all datasets
print ('Dimension of creditcard_original_leftover:'+str(creditcard_original_leftover.shape))
print('Dimension of creditcard_undersampled:',str(creditcard_undersampled.shape))
print('Dimension of creditcard:',str(credit_card_ss.shape))
#We now have an undersample which consists 50% of fraudulent and non fraudulent transactions. 
#We will train both supervised and unsupervised learning classifers on this undersampled dataset, then compare their performances
#on the original dataset.
#Some of the classifiers from the supervised learning families that will be used are:
#1.logistic regression((penalty=’l2’, dual=False, tol=0.0001, C=1.0, fit_intercept=True, intercept_scaling=1, class_weight=None, random_state=None, 
#solver=’warn’, max_iter=100, multi_class=’warn’, verbose=0, warm_start=False, n_jobs=None))
#2.decision tree (criterion=’gini’, splitter=’best’, max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features=None,
#random_state=None, max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, class_weight=None, presort=False)
#3.SVC ((C=1.0, kernel=’rbf’, degree=3, gamma=’auto_deprecated’, coef0=0.0, shrinking=True, probability=False, tol=0.001, 
#cache_size=200, class_weight=None, verbose=False, max_iter=-1, decision_function_shape=’ovr’, random_state=None))
#Splitting undersampled dataset 
from sklearn.model_selection import train_test_split
X_under = creditcard_undersampled.drop('Class', axis=1)
y_under = creditcard_undersampled['Class']
X_under_train, X_under_test, y_under_train, y_under_test = train_test_split(X_under, y_under, test_size=0.3, random_state=55)
#Use logistic regression on undersampeld dataset and get the scores. 
#Will use gridsearch as well to see which hyperparameters give the best recall and f1 scores. 
from sklearn.model_selection import GridSearchCV
​
parameter_candidates = [
  {'penalty':['l2'],'C': [0.001,0.01,0.1,1, 10, 100, 1000,10000]},
  {'penalty':['l1'],'C': [0.001,0.01,0.1,1, 10, 100, 1000,10000]},
]
cv_range=[3,5,10,15,20]
for i in cv_range:
    lr_under = GridSearchCV(estimator=LogisticRegression(random_state=90), param_grid=parameter_candidates, n_jobs=-1, cv=i)
​
#Training set 
    lr_under.fit(X_under_train,y_under_train)
​
#Prediction on training set - undersampled 
    pred_y_lr_under = lr_under.predict(X_under_test)
​
#Getting accuracy scores on the undersampled dataset 
#Accuracy and Recall scores. 
print ('Accuracy score of logistic regression classifier on test set:{:.3f}'.format(accuracy_score(y_under_test,pred_y_lr_under)))
print ('Recall score of logistic regression classifier on test set:{:.3f}'.format(recall_score(y_under_test,pred_y_lr_under)))
print ('F1 score of logistic regression classifier on test set:{:.3f}'.format(f1_score(y_under_test,pred_y_lr_under)))
print()
cm_lr_under = confusion_matrix(y_under_test,pred_y_lr_under)
print('Confusion matrix with logistic regression classifier with on undersampled test set:\n%s' % cm_lr_under)
print()
print('Best C for logistic regression:',lr_under.best_estimator_.C) 
print('Best penalty:',lr_under.best_estimator_.penalty)
print('The best paramaters for the logistic regression classifier according to GridSearch and CV = %r:'% (i),lr_under.best_params_)
​
#Test on original dataset without the dataset used for the undersampled set 
#Prediction on original creditcard dataset 
X_original = creditcard_original_leftover.drop('Class', axis=1)
y_original = creditcard_original_leftover['Class']
​
pred_y_original_lr_under = lr_under.predict(X_original)
​
#Since this original dataset is an imbalanced dataset, the accuracy scores will be biased towards the majority class. Will use 
#recall, f1 scores and confusion matrix
print ('Recall score of logistic regression classifier on original set:{:.3f}'.format(recall_score(y_original,pred_y_original_lr_under)))
print ('F1 score of logistic regression classifier on original set:{:.3f}'.format(f1_score(y_original,pred_y_original_lr_under)))
print()
cm_lr_under_original = confusion_matrix(y_original,pred_y_original_lr_under)
print('Confusion matrix with logistic regression classifier with on original set:\n%s' % cm_lr_under_original)
print()
#using decision tree classifier on undersampled training set
#Will use gridsearch as well to see which hyperparameters give the best recall and f1 scores. 
​
parameter_candidates = {'max_features': ['log2', 'sqrt','auto'], 
              'criterion': ['entropy', 'gini'],
              'max_depth': [2, 3, 5, 10], 
              'min_samples_split': [2, 3, 5],
              'min_samples_leaf': [1,5,8]
             }
cv_range=[3,5,10,15,20]
for i in cv_range:
    dt_under = GridSearchCV(estimator=DecisionTreeClassifier(random_state=75), param_grid=parameter_candidates, n_jobs=-1, cv=i)
​
#Training set 
    dt_under.fit(X_under_train,y_under_train)
​
#Prediction on training set - undersampled 
    pred_y_dt_under = dt_under.predict(X_under_test)
​
#Getting accuracy scores on the undersampled dataset 
#Accuracy and Recall scores. 
print ('Accuracy score of decision tree classifier on test set:{:.3f}'.format(accuracy_score(y_under_test,pred_y_dt_under)))
print ('Recall score of decision tree classifier on test set:{:.3f}'.format(recall_score(y_under_test,pred_y_dt_under)))
print ('F1 score of decision tree classifier on test set:{:.3f}'.format(f1_score(y_under_test,pred_y_dt_under)))
print()
cm_dt_under = confusion_matrix(y_under_test,pred_y_dt_under)
print('Confusion matrix with decision tree classifier with on undersampled test set:\n%s' % cm_dt_under)
print()
print('The best paramaters for the decision tree classifier according to GridSearch and CV = %r:'% (i),dt_under.best_params_)
#Testing decision tree classifier on original dataset 
pred_y_original_dt_under = dt_under.predict(X_original)
​
#Since this original dataset is an imbalanced dataset, the accuracy scores will be biased towards the majority class. Will use 
#recall, f1 scores and confusion matrix
print ('Recall score of decision tree classifier on original set:{:.3f}'.format(recall_score(y_original,pred_y_original_dt_under)))
print ('F1 score of decision tree classifier on original set:{:.3f}'.format(f1_score(y_original,pred_y_original_dt_under)))
print()
cm_dt_under_original = confusion_matrix(y_original,pred_y_original_dt_under)
print('Confusion matrix with decision tree classifier on original set:\n%s' % cm_dt_under_original)
print()
#Using SVM classifier 
#Will use gridsearch as well to see which hyperparameters give the best recall and f1 scores. 
from sklearn import svm
parameter_candidates = {'C': [0.001, 0.01, 1, 10, 100, 1000], 
              'kernel': ['rbf', 'linear','poly'],
              'gamma': [0.001, 0.01, 1, 10, 100, 1000] 
             }
cv_range=[3,5,10,15,20]
for i in cv_range:
    svc_under = GridSearchCV(estimator=svm.SVC(random_state=60), param_grid=parameter_candidates, n_jobs=-1, cv=i)
​
#Training set 
    svc_under.fit(X_under_train,y_under_train)
​
#Prediction on training set - undersampled 
    pred_y_svc_under = svc_under.predict(X_under_test)
​
#Getting accuracy scores on the undersampled dataset 
#Accuracy and Recall scores. 
print ('Accuracy score of SVM classifier on test set:{:.3f}'.format(accuracy_score(y_under_test,pred_y_svc_under)))
print ('Recall score of SVM classifier on test set:{:.3f}'.format(recall_score(y_under_test,pred_y_svc_under)))
print ('F1 score of SVM classifier on test set:{:.3f}'.format(f1_score(y_under_test,pred_y_svc_under)))
print()
cm_svc_under = confusion_matrix(y_under_test,pred_y_svc_under)
print('Confusion matrix with SVM classifier with on undersampled test set:\n%s' % cm_svc_under)
print()
print('The best paramaters for the SVM classifier according to GridSearch and CV = %r:'% (i),svc_under.best_params_)
#Testing decision tree classifier on original dataset 
pred_y_original_svc_under = svc_under.predict(X_original)
​
#Since this original dataset is an imbalanced dataset, the accuracy scores will be biased towards the majority class. Will use 
#recall, f1 scores and confusion matrix
print ('Recall score of SVM classifier on original set:{:.3f}'.format(recall_score(y_original,pred_y_original_svc_under)))
print ('F1 score of SVM classifier on original set:{:.3f}'.format(f1_score(y_original,pred_y_original_svc_under)))
print()
cm_svc_under_original = confusion_matrix(y_original,pred_y_original_svc_under)
print('Confusion matrix with SVM classifier on original set:\n%s' % cm_svc_under_original)
#use ensemble algorithm without hyperparameter tuning to see if there is any improvement in F1 score. Will use adaboost 
#in combination with decision tree classifier. 
from sklearn.ensemble import AdaBoostClassifier
bdt = AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),
                         algorithm="SAMME",
                         n_estimators=200,random_state = 55)
​
#Training set 
bdt.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_bdt=bdt.predict(X_credit_card_ss_test)
​
print ('Recall score of AdaBoosted tree decision classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_bdt)))
print ('F1 score of AdaBoosted tree decision classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_bdt)))
print()
cm_bdt_original = confusion_matrix(y_credit_card_ss_test,pred_y_bdt)
print('Confusion matrix with AdaBoosted tree decision classifier on original set:\n%s' % cm_bdt_original)
print()
​
#use ensemble algorithm without hyperparameter tuning to see if there is any improvement in F1 score. Will use adaboost 
#in combination with decision tree classifier. 
from sklearn.ensemble import AdaBoostClassifier
bdt = AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),
                         algorithm="SAMME",
                         n_estimators=300,random_state = 55)
​
#Training set 
bdt.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_bdt=bdt.predict(X_credit_card_ss_test)
​
print ('Recall score of AdaBoosted tree decision classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_bdt)))
print ('F1 score of AdaBoosted tree decision classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_bdt)))
print()
cm_bdt_original = confusion_matrix(y_credit_card_ss_test,pred_y_bdt)
print('Confusion matrix with AdaBoosted tree decision classifier on original set:\n%s' % cm_bdt_original)
print()
#Use Adaboost classifier by itself and see the performance 
from sklearn.ensemble import AdaBoostClassifier
ada = AdaBoostClassifier(n_estimators=100, random_state=90)
​
#Training set 
ada.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_ada=ada.predict(X_credit_card_ss_test)
​
print ('Recall score of AdaBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_ada)))
print ('F1 score of AdaBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_ada)))
print()
cm_ada_original = confusion_matrix(y_credit_card_ss_test,pred_y_ada)
print('Confusion matrix with AdaBoost Classifier on original set:\n%s' % cm_ada_original)
print()
#Use Adaboost classifier by itself and see the performance 
from sklearn.ensemble import AdaBoostClassifier
ada = AdaBoostClassifier(n_estimators=200, random_state=90)
​
#Training set 
ada.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_ada=ada.predict(X_credit_card_ss_test)
​
print ('Recall score of AdaBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_ada)))
print ('F1 score of AdaBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_ada)))
print()
cm_ada_original = confusion_matrix(y_credit_card_ss_test,pred_y_ada)
print('Confusion matrix with AdaBoost Classifier on original set:\n%s' % cm_ada_original)
print()
#Use GradientBoosting classifier by itself and see the performance 
from sklearn.ensemble import GradientBoostingClassifier
gbm = GradientBoostingClassifier(n_estimators=200, random_state=90)
​
#Training set 
gbm.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_gbm=gbm.predict(X_credit_card_ss_test)
​
print ('Recall score of GradientBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_gbm)))
print ('F1 score of GradientBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_gbm)))
print()
cm_gbm_original = confusion_matrix(y_credit_card_ss_test,pred_y_gbm)
print('Confusion matrix with GradientBoost Classifier on original set:\n%s' % cm_gbm_original)
print()
#Use GradientBoosting classifier by itself and see the performance 
from sklearn.ensemble import GradientBoostingClassifier
gbm = GradientBoostingClassifier(n_estimators=300, random_state=90)
​
#Training set 
gbm.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_gbm=gbm.predict(X_credit_card_ss_test)
​
print ('Recall score of GradientBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_gbm)))
print ('F1 score of GradientBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_gbm)))
print()
cm_gbm_original = confusion_matrix(y_credit_card_ss_test,pred_y_gbm)
print('Confusion matrix with GradientBoost Classifier on original set:\n%s' % cm_gbm_original)
print()
#Use extreme gradient boosting to see if there is any improvement in scores in comparison to gradient boosting -no hyperparameter tuning 
#Use GradientBoosting classifier by itself and see the performance 
import xgboost as xgb
xgb = xgb.XGBClassifier(n_estimators=300 ,random_state = 105)
​
#Training set 
xgb.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_xgb=xgb.predict(X_credit_card_ss_test)
​
print ('Recall score of Extreme GradientBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_xgb)))
print ('F1 score of Extreme GradientBoost GradientBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_xgb)))
print()
cm_xgb_original = confusion_matrix(y_credit_card_ss_test,pred_y_xgb)
print('Confusion matrix with Extreme GradientBoost Classifier on original set:\n%s' % cm_xgb_original)
print()
#Use extreme gradient boosting to see if there is any improvement in scores in comparison to gradient boosting -no hyperparameter tuning 
#Use GradientBoosting classifier by itself and see the performance 
import xgboost as xgb
xgb = xgb.XGBClassifier(n_estimators=200 ,random_state = 105)
​
#Training set 
xgb.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_xgb=xgb.predict(X_credit_card_ss_test)
​
print ('Recall score of Extreme GradientBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_xgb)))
print ('F1 score of Extreme GradientBoost GradientBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_xgb)))
print()
cm_xgb_original = confusion_matrix(y_credit_card_ss_test,pred_y_xgb)
print('Confusion matrix with Extreme GradientBoost Classifier on original set:\n%s' % cm_xgb_original)
print()
#Use random forest on dataset to get recall and F1 scores 
from sklearn.ensemble import RandomForestClassifier 
rf = RandomForestClassifier(n_estimators=100 ,criterion = 'gini', random_state = 30)
​
#Training set 
rf.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_rf=rf.predict(X_credit_card_ss_test)
​
print ('Recall score of Random Forest classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_rf)))
print ('F1 score of Random Forest classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_rf)))
print()
cm_rf_original = confusion_matrix(y_credit_card_ss_test,pred_y_rf)
print('Confusion matrix with RandomForestClassifier Classifier on original set:\n%s' % cm_rf_original)
print()
#Use random forest on dataset to get recall and F1 scores 
from sklearn.ensemble import RandomForestClassifier 
rf = RandomForestClassifier(n_estimators=200 ,criterion = 'gini', random_state = 30)
​
#Training set 
rf.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_rf=rf.predict(X_credit_card_ss_test)
​
print ('Recall score of Random Forest classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_rf)))
print ('F1 score of Random Forest classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_rf)))
print()
cm_rf_original = confusion_matrix(y_credit_card_ss_test,pred_y_rf)
print('Confusion matrix with RandomForestClassifier Classifier on original set:\n%s' % cm_rf_original)
print()
#Use ensemble algorithms to see how they perform on original dataset. Supposedly, they can handle imbalanced dataset
#Adaboost - works by weighting instances in the dataset by how easy or difficult they are to classify, allowing the algorithm 
#to pay or or less attention to them in the construction of subsequent models.
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import GridSearchCV
​
parameter_candidates = {'n_estimators': [50, 200],  
              'learning_rate': [0.01, 100]
             }
 
cv_range=[10,20]
for i in cv_range:
    ada_gs = GridSearchCV(estimator=AdaBoostClassifier(random_state = 50), param_grid=parameter_candidates, n_jobs=-1, cv=i)
    
#Training set 
ada_gs.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_ada_gs=ada_gs.predict(X_credit_card_ss_test)
​
print ('Recall score of AdaBoost classifier with gridsearch on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_ada_gs)))
print ('F1 score of AdaBoost classifier with gridsearch on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_ada_gs)))
print(\n)
cm_ada_gs_original = confusion_matrix(y_credit_card_ss_test,pred_y_ada_gs)
print('Confusion matrix with AdaBoost classifier with gridsearch on original set:\n%s' % cm_ada_gs_original)
print(\n)
print('The best paramaters for the AdaBoost classifier according to GridSearch and CV = %r:'% (i),ada_gs.best_params_)
​
​
​
​
#Use GradientBoosting classifier
from sklearn.ensemble import GradientBoostingClassifier
parameter_candidates = {'n_estimators': [50,200], 
              'learning_rate': [0.01, 1, 10, 100]
             }
 
cv_range=[10,20]
for i in cv_range:
    gbm = GridSearchCV(estimator=GradientBoostingClassifier(random_state = 55), param_grid=parameter_candidates, n_jobs=-1, cv=i)
    
#Training set 
gbm.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_gbm=gbm.predict(X_credit_card_ss_test)
​
print ('Recall score of GradientBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_gbm)))
print ('F1 score of GradientBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_gbm)))
print()
cm_gbm_original = confusion_matrix(y_credit_card_ss_test,pred_y_gbm)
print('Confusion matrix with GradientBoost classifier on original set:\n%s' % cm_gbm_original)
print()
#print (ConfusionMatrix(y_credit_card_ss_test,pred_y_gbm))
#Use XGB (extreme gradient boosting)
import xgboost as xgb
parameter_candidates = {'n_estimators': [50, 200], 
              'learning_rate': [ 0.01, 100]
             }
cv_range=[10,20]
for i in cv_range:
    xgb = GridSearchCV(estimator=xgb.XGBClassifier(random_state = 105), param_grid=parameter_candidates, n_jobs=-1, cv=i)
    
#Training set 
xgb.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_xgb=xgb.predict(X_credit_card_ss_test)
​
print ('Recall score of XGBoost classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_xgb)))
print ('F1 score of XGBBoost classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_xgb)))
print()
cm_xgb_original = confusion_matrix(y_credit_card_ss_test,pred_y_xgb)
print('Confusion matrix with XGBBoost classifier on original set:\n%s' % cm_xgb_original)
print()
​
#Using random forest as another technique 
from sklearn.ensemble import RandomForestClassifier 
parameter_candidates = {'n_estimators': [50,200], 
              'criterion': ['gini','entropy'],
              'max_features': ['auto','sqrt','log2'],
              'class_weight': ['none','balanced']        
             }
cv_range=[10,20]
for i in cv_range:
    rf = GridSearchCV(estimator=RandomForestClassifier(random_state = 105), param_grid=parameter_candidates, n_jobs=-1, cv=i)
    
#Training set 
rf.fit(X_credit_card_ss_train, y_credit_card_ss_train)
​
#Test set
pred_y_rf=rf.predict(X_credit_card_ss_test)
​
print ('Recall score of RandomForest classifier on original set:{:.3f}'.format(recall_score(y_credit_card_ss_test,pred_y_rf)))
print ('F1 score of RandomForest classifier on original set:{:.3f}'.format(f1_score(y_credit_card_ss_test,pred_y_rf)))
print()
cm_rf_original = confusion_matrix(y_credit_card_ss_test,pred_y_rf)
print('Confusion matrix with RandomForest classifier on original set:\n%s' % cm_rf_original)
print()
​
​
​
#Using isolation forest algorithm. It seems that isolated forest is suited for anomaly detection. 
#IsolationForest ‘isolates’ observations by randomly selecting a feature and then randomly selecting a 
#split value between the maximum and minimum values of the selected feature.Since recursive partitioning can be represented by a tree structure, the number of splittings required to isolate a sample is equivalent to the path length from the root node to the terminating node.
#This path length, averaged over a forest of such random trees, is a measure of normality and our decision function.
#Random partitioning produces noticeable shorter paths for anomalies. Hence, when a forest of random trees collectively
#produce shorter path lengths for particular samples, they are highly likely to be anomalies.(https://scikit-learn.org/stable/auto_examples/ensemble/plot_isolation_forest.html)
from sklearn.ensemble import IsolationForest
#Creating a training dataset containing only non fraud cases taken from the dataset 
X2_train = resample(credit_card_ss[credit_card_ss.Class==0], 
                                 replace=False,    # sample without replacement
                                 n_samples=199000,     # represent around ~70% of non-fraudulent cases
                                 random_state=75) # reproducible results
​
X2_isof = X2_train.drop('Class', axis = 1)
​
#Creating a 2nd dataset containing left over data after 199000 cases of non fraud were sampled out. 
X_original_leftover = credit_card_ss.loc[~credit_card_ss.set_index(list(credit_card_ss.columns)).index.isin(X2_train.set_index(list(X2_train.columns)).index)]
X_original_no_y = X_original_leftover.drop('Class', axis =1)
y_from_X_original = X_original_leftover ['Class']
#Training set 
isof = IsolationForest(behaviour='new',max_samples='auto',random_state=45)
isof.fit(X2_isof)
​
#Test set
output_pred_y_score=pd.DataFrame((isof.predict(X_original_no_y)),columns =['isolation forest score'])
​
X_original_leftover = X_original_leftover.reset_index() #Resetting index 
X_original_leftover.head(10)
X_original_leftover=X_original_leftover.drop(['index'],axis=1)
original_leftover  = pd.concat([X_original_leftover, output_pred_y_score], axis=1)
#original_leftover = X_original_leftover.join(output_pred_y_score).reset_index()
pd.value_counts(original_leftover['isolation forest score'].values, sort=False) #checking the  count per socre (1, -1)
pd.value_counts(original_leftover['Class'].values, sort=False) #Checking the count per classes 
original_leftover.shape
#checking the dataframe which has both class and isolation forest score 
original_leftover.head(5)
original_leftover.shape
#To be able to get recall and F1 scores, I will have to do a count based on conditions: 
#Class =1 and Isolation forest score = 1 (Real Fraud but classified as non fraud, False negative)
#Class = 1 and Isolation forest score =-1 (Real Fraud but classified as fraud, True positive)
#Class = 0 and Isolation Forest score =1 (Real non fraud and classified as non fraud, True negative)
#Class = 0 and Isolation Forest score =-1 (Read non fraud but classified as fraud, False positive )
​
True_Negative = len(original_leftover[(original_leftover['Class']==0) & (original_leftover['isolation forest score']==1)])
False_Positive = len(original_leftover[(original_leftover['Class']==0) & (original_leftover['isolation forest score']==-1.0)])
True_Positive = len(original_leftover[(original_leftover['Class']==1) & (original_leftover['isolation forest score']==-1)])
False_Negative = len(original_leftover[(original_leftover['Class']==1) & (original_leftover['isolation forest score']==1)])
​
print('Number of fraud cases classified as non fraud with isolation forest is', 
      len(original_leftover[(original_leftover['Class']==1) & (original_leftover['isolation forest score']==1)]))
print('Number of fraud cases classified as fraud with isolation forest is', 
      len(original_leftover[(original_leftover['Class']==1) & (original_leftover['isolation forest score']==-1)]))
print('Number of non fraud cases classified as fraud with isolation forest is', 
      len(original_leftover[(original_leftover['Class']==0) & (original_leftover['isolation forest score']==-1)]))
print('Number of non fraud cases classified as non fraud with isolation forest is', 
      len(original_leftover[(original_leftover['Class']==0) & (original_leftover['isolation forest score']==1)]))
Recall_score = True_Positive/(True_Positive + False_Negative)
Precision_score = True_Positive/(True_Positive + False_Positive)
F1_score = 2*(Precision_score*Recall_score)/(Precision_score+Recall_score)
print ('Recall score with isolation forest is {:.3f}'.format(Recall_score))
print ('F1_score with isolation forest is {:.3f}'.format(F1_score))
#Printing True_Negative, False_Positive, False_Negative, True_Positive as a confusion matrix for the isolation forest model
print ('[[76452   8456]')
print ( '[   49     443]]')
